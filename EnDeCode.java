import java.util.Scanner;

public class EnDeCode {
    static int a = 0;
    static boolean enc = false;
    static String[] L = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9",",",".","!","?",":",";"," ","#","$","%","&","(",")","[","]","{","}","+","-","*","/","<",">","="};
    static int length = L.length;

    public static int findIndex(String letter){
        for(int i = 0; i < length; i++){
            if(L[i].equals(letter)){
                return i;
            }
        }
        return 0;
    }

    public static String endecode(String letter){
        int c = 0;
        int b = findIndex(letter);
        if (enc){
            a = (b+a)%length;
            return L[a];
        }
        else{
            c = (b-a+length)%length;
            a = b;
            return L[c];
        }
    }

    public static void main(String[] args){
        Scanner my_input = new Scanner(System.in);
        System.out.print("Choose en/de: ");
        String inp = my_input.nextLine();
        enc = (inp.equals("+") || inp.equals("enc") || inp.equals("en") || inp.equals("e") || inp.equals("1"));
        System.out.println("Enter the text: ");
        String encoded = "";
        inp = my_input.nextLine();
        while(inp != ""){
            String[] letters = inp.split("", 0);
            for(String letter : letters){
                encoded += endecode(letter);
            }
            inp = my_input.nextLine();
        }
        System.out.println(encoded);
        my_input.close();
    }
}