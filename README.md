# About

Given any Input the Programm adds/subtracts all characters in that Input therefor making it deterministically unreadable.
Choosing the other Option and entering the new unreadable text, will turn it back. can be used recursivly.
The Key is important to decode, it beeing: the amount and order of the readable Alphabet (n! possibilities), the first Letter used, the amount of recursion and the Option of +/-, this however includes similar keys and mirrored keys, so the actual number of keys is a bit lower.

# Why

Fun, mainly.

# Recursion

while not implemented here, it is fun and adds a sheer infinite amount of keys, simply because recursion can be done on all of the Text, or on the letters given their place or size or just a pseudo-random amount given any deterministic algorithm.
