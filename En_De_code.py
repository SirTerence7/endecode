import string
L = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","ß","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9",",",".","!","?",":",";"," ","#","$","%","&","(",")","[","]","{","}","+","-","*","/","<",">","=","☹"]

enc = False
length = len(L)
Input = []
a = 0

def code(letter: string):
    '''
    changes the letter to given previous letters
    '''
    global enc
    global a

    if letter.lower() not in L:
        return letter
    #up = letter.isupper()
    #letter = letter.lower()

    if enc:
        a += L.index(letter)
        if a >= length:
            a -= length
        return L[a]#.upper() if up else L[a]

    else:
        b = L.index(letter)
        c = b-a
        if c < 0:
            c += length
        a = b
        return L[c]#.upper() if up else L[c]


print("Choose en/de: ")

inp = str(input())
if inp == "1" or inp == "encode" or inp == "en" or inp == "+":
    enc = True

print("Enter the text: ")

#get the input and en/de code it
while inp != "" or inp2 != "":
    Input.append(inp)
    temp_inp = list(str(input()))
    temp_inp = [code(a) for a in temp_inp]
    inp = "".join(temp_inp)
    inp2 = inp

#print the result
for text in Input:
    print(text)