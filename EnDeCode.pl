use warnings;
use strict;
use List::Util qw(first);

my@L=("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","ß","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9",",",".","!","?",":",";"," ","#","\$","%","&","(",")","[","]","{","}","+","-","*","/","<",">","=","☹");# accepted Elements
print("en/de? ");chomp(my$I=<STDIN>); # Choice
print("Enter: ");
my$O;my$a=0;my$b;my$c;my$e=($I eq"en"||$I eq"+"); # "Bool"
until ($I eq""){
	chomp($I=<STDIN>); # Input
	foreach $c(split(//,$I)){ # Handle single Letters
		$b=first{$L[$_]eq$c}0..$#L; # get Index from Element
		$c=(($e*2-1)*$a+$b)%$#L;
		$a=$e*$c+(1-$e)*$b;
		$O.=$L[$c]; # get Element of Index and concatate to Output
	}
	$O.="\n";
}
print($O);
